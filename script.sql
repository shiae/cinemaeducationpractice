USE [ShirkunovISP31Cinema]
GO
/****** Object:  Table [dbo].[Cinema]    Script Date: 15.05.2023 0:27:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cinema](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NameCinema] [nvarchar](250) NULL,
	[INNCinema] [int] NULL,
	[Address] [nvarchar](250) NULL,
	[Chief] [nvarchar](250) NULL,
	[PhoneChief] [nvarchar](250) NULL,
	[Owner] [nvarchar](250) NULL,
	[PhoneOwner] [nvarchar](250) NULL,
	[Phone] [nvarchar](250) NULL,
	[District] [nvarchar](250) NULL,
	[BankCinema] [nvarchar](250) NULL,
	[AccountCinema] [int] NULL,
	[Capacity] [int] NULL,
 CONSTRAINT [PK_Cinema] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Film]    Script Date: 15.05.2023 0:27:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Film](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NameFilm] [nvarchar](250) NULL,
	[Author] [nvarchar](250) NULL,
	[Comment] [nvarchar](250) NULL,
	[Producer] [nvarchar](250) NULL,
	[Company] [nvarchar](250) NULL,
	[Year] [int] NULL,
	[Land] [nvarchar](250) NULL,
	[Time] [int] NULL,
	[Cost] [int] NULL,
	[Rate] [int] NULL,
 CONSTRAINT [PK_Film] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Provider]    Script Date: 15.05.2023 0:27:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Provider](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NameProvider] [nvarchar](250) NULL,
	[INN] [int] NULL,
	[Address] [nvarchar](250) NULL,
	[Bank] [nvarchar](250) NULL,
	[Account] [int] NULL,
	[Sign] [bit] NULL,
 CONSTRAINT [PK_Provider] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Purchase]    Script Date: 15.05.2023 0:27:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Purchase](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdProvider] [int] NULL,
	[IdFilm] [int] NULL,
	[DateBuy] [date] NULL,
	[Summ] [money] NULL,
 CONSTRAINT [PK_Purchase] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Rent]    Script Date: 15.05.2023 0:27:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rent](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DateStart] [date] NULL,
	[DateStop] [date] NULL,
	[Worker] [nvarchar](250) NULL,
	[PhoneWorker] [nvarchar](250) NULL,
	[Summa] [money] NULL,
	[Tax] [money] NULL,
	[IdCinema] [int] NULL,
	[IdFilm] [int] NULL,
 CONSTRAINT [PK_Rent] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Cinema] ON 

INSERT [dbo].[Cinema] ([Id], [NameCinema], [INNCinema], [Address], [Chief], [PhoneChief], [Owner], [PhoneOwner], [Phone], [District], [BankCinema], [AccountCinema], [Capacity]) VALUES (1, N'Ленкомfef', 555555, N'г. Вологда, ул. Петькина', N'Вася Пупкин', N'+79581532343', N'Игорь Дудкин', N'+79582564747', N'+79582564747', N'Заречье', N'Сбербанк', 44444, 12)
INSERT [dbo].[Cinema] ([Id], [NameCinema], [INNCinema], [Address], [Chief], [PhoneChief], [Owner], [PhoneOwner], [Phone], [District], [BankCinema], [AccountCinema], [Capacity]) VALUES (2, N'Синема Парк', 555555, N'г. Вологда, ул. Петькина', N'Петя Барбосов', N'+79581532343', N'Дима Думов', N'+79582564747', N'+79582564747', N'Заречье', N'Сбербанк', 76767, 12)
SET IDENTITY_INSERT [dbo].[Cinema] OFF
GO
SET IDENTITY_INSERT [dbo].[Film] ON 

INSERT [dbo].[Film] ([Id], [NameFilm], [Author], [Comment], [Producer], [Company], [Year], [Land], [Time], [Cost], [Rate]) VALUES (1, N'Форсаж 8', N'Ф. Гэри Грей', N'Пристегните ремни — гонка продолжается.', N'Ф. Гэри Грей', N'Universal Pictures', 2017, N'США', 136, 250, 8)
INSERT [dbo].[Film] ([Id], [NameFilm], [Author], [Comment], [Producer], [Company], [Year], [Land], [Time], [Cost], [Rate]) VALUES (2, N'Форсаж 9', N'Джастин Лин', N'Доминик Торетто ведет спокойную жизнь в глуши вместе с Летти и сыном Брайаном, но опасность всегда где-то рядом. ', N'Джастин Лин', N'Universal Pictures', 2021, N'США', 143, 250, 9)
SET IDENTITY_INSERT [dbo].[Film] OFF
GO
SET IDENTITY_INSERT [dbo].[Provider] ON 

INSERT [dbo].[Provider] ([Id], [NameProvider], [INN], [Address], [Bank], [Account], [Sign]) VALUES (1, N'Артём Ширкунов', 770708, N'г. Вологда, ул. Пупкова 31', N'Сбербанк', 212332, 1)
INSERT [dbo].[Provider] ([Id], [NameProvider], [INN], [Address], [Bank], [Account], [Sign]) VALUES (2, N'Бобров Николай', 770708, N'г. Вологда, ул. Пупкова 31', N'Сбербанк', 434343, 0)
INSERT [dbo].[Provider] ([Id], [NameProvider], [INN], [Address], [Bank], [Account], [Sign]) VALUES (3, N'Петя Петров', 770708, N'г. Вологда, ул. Пупкова 31', N'Сбербанк', 323232, 1)
SET IDENTITY_INSERT [dbo].[Provider] OFF
GO
SET IDENTITY_INSERT [dbo].[Purchase] ON 

INSERT [dbo].[Purchase] ([Id], [IdProvider], [IdFilm], [DateBuy], [Summ]) VALUES (1, 1, NULL, CAST(N'2023-05-14' AS Date), 250.0000)
INSERT [dbo].[Purchase] ([Id], [IdProvider], [IdFilm], [DateBuy], [Summ]) VALUES (2, 2, 2, CAST(N'2023-05-14' AS Date), 250.0000)
INSERT [dbo].[Purchase] ([Id], [IdProvider], [IdFilm], [DateBuy], [Summ]) VALUES (3, 3, NULL, CAST(N'2023-05-14' AS Date), 250.0000)
SET IDENTITY_INSERT [dbo].[Purchase] OFF
GO
SET IDENTITY_INSERT [dbo].[Rent] ON 

INSERT [dbo].[Rent] ([Id], [DateStart], [DateStop], [Worker], [PhoneWorker], [Summa], [Tax], [IdCinema], [IdFilm]) VALUES (1, CAST(N'2023-04-14' AS Date), CAST(N'2023-04-18' AS Date), N'Игорь Петров', N'+79584345737', 250.0000, 5.0000, 2, NULL)
INSERT [dbo].[Rent] ([Id], [DateStart], [DateStop], [Worker], [PhoneWorker], [Summa], [Tax], [IdCinema], [IdFilm]) VALUES (2, CAST(N'2023-04-14' AS Date), CAST(N'2023-04-18' AS Date), N'Дима Кукушев', N'+79573647383', 250.0000, 5.0000, 2, 2)
INSERT [dbo].[Rent] ([Id], [DateStart], [DateStop], [Worker], [PhoneWorker], [Summa], [Tax], [IdCinema], [IdFilm]) VALUES (3, CAST(N'2023-04-14' AS Date), CAST(N'2023-04-18' AS Date), N'Алена Пупкова', N'+79253647574', 250.0000, 5.0000, NULL, NULL)
INSERT [dbo].[Rent] ([Id], [DateStart], [DateStop], [Worker], [PhoneWorker], [Summa], [Tax], [IdCinema], [IdFilm]) VALUES (5, CAST(N'2023-05-15' AS Date), CAST(N'2023-05-25' AS Date), N'fewfwe', N'fewfew', 255.0000, 25.0000, 2, 1)
INSERT [dbo].[Rent] ([Id], [DateStart], [DateStop], [Worker], [PhoneWorker], [Summa], [Tax], [IdCinema], [IdFilm]) VALUES (6, CAST(N'2023-05-15' AS Date), CAST(N'2023-05-25' AS Date), N'fewfew', N'fewfew', 255.0000, 25.0000, 2, 2)
INSERT [dbo].[Rent] ([Id], [DateStart], [DateStop], [Worker], [PhoneWorker], [Summa], [Tax], [IdCinema], [IdFilm]) VALUES (7, CAST(N'2022-05-15' AS Date), CAST(N'2022-05-25' AS Date), N'fgewfew', N'fewfwe', 0.9146, 25.0000, 2, 2)
INSERT [dbo].[Rent] ([Id], [DateStart], [DateStop], [Worker], [PhoneWorker], [Summa], [Tax], [IdCinema], [IdFilm]) VALUES (8, CAST(N'2005-05-20' AS Date), CAST(N'2005-05-25' AS Date), N'апуцауц', N'ауцауц', 0.6600, 5.0000, 1, 2)
INSERT [dbo].[Rent] ([Id], [DateStart], [DateStop], [Worker], [PhoneWorker], [Summa], [Tax], [IdCinema], [IdFilm]) VALUES (9, CAST(N'2005-05-10' AS Date), CAST(N'2005-05-25' AS Date), N'fgewfew', N'fewfew', 9.9000, 25.0000, 1, 2)
SET IDENTITY_INSERT [dbo].[Rent] OFF
GO
ALTER TABLE [dbo].[Purchase]  WITH CHECK ADD  CONSTRAINT [FK_Purchase_Film] FOREIGN KEY([IdFilm])
REFERENCES [dbo].[Film] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Purchase] CHECK CONSTRAINT [FK_Purchase_Film]
GO
ALTER TABLE [dbo].[Purchase]  WITH CHECK ADD  CONSTRAINT [FK_Purchase_Provider] FOREIGN KEY([IdProvider])
REFERENCES [dbo].[Provider] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Purchase] CHECK CONSTRAINT [FK_Purchase_Provider]
GO
ALTER TABLE [dbo].[Rent]  WITH CHECK ADD  CONSTRAINT [FK_Rent_Cinema] FOREIGN KEY([IdCinema])
REFERENCES [dbo].[Cinema] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Rent] CHECK CONSTRAINT [FK_Rent_Cinema]
GO
ALTER TABLE [dbo].[Rent]  WITH CHECK ADD  CONSTRAINT [FK_Rent_Film] FOREIGN KEY([IdFilm])
REFERENCES [dbo].[Film] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Rent] CHECK CONSTRAINT [FK_Rent_Film]
GO
