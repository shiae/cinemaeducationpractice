using System.Data.Entity;

namespace ShirkunovISP31Cinema
{
    public partial class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext()
            : base("name=ConnectionString")
        {
        }

        public virtual DbSet<Cinema> Cinemas { get; set; }
        public virtual DbSet<Film> Films { get; set; }
        public virtual DbSet<Provider> Providers { get; set; }
        public virtual DbSet<Purchase> Purchases { get; set; }
        public virtual DbSet<Rent> Rents { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Cinema>()
                .HasMany(e => e.Rents)
                .WithOptional(e => e.Cinema)
                .HasForeignKey(e => e.IdCinema);

            modelBuilder.Entity<Film>()
                .HasMany(e => e.Purchases)
                .WithOptional(e => e.Film)
                .HasForeignKey(e => e.IdFilm);

            modelBuilder.Entity<Film>()
                .HasMany(e => e.Rents)
                .WithOptional(e => e.Film)
                .HasForeignKey(e => e.IdFilm);

            modelBuilder.Entity<Provider>()
                .HasMany(e => e.Purchases)
                .WithOptional(e => e.Provider)
                .HasForeignKey(e => e.IdProvider);

            modelBuilder.Entity<Purchase>()
                .Property(e => e.Summ)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Rent>()
                .Property(e => e.Summa)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Rent>()
                .Property(e => e.Tax)
                .HasPrecision(19, 4);
        }
    }
}
