using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ShirkunovISP31Cinema
{

    [Table("Cinema")]
    public partial class Cinema : BaseEntity
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Cinema()
        {
            Rents = new HashSet<Rent>();
        }
        [Display(Name = "�������� ����������")]
        [StringLength(255)]
        public string NameCinema { get; set; }

        [Display(Name = "��� ����������")]
        public int? INNCinema { get; set; }

        [Display(Name = "����� ����������")]
        [StringLength(255)]
        public string Address { get; set; }

        [Display(Name = "�������� ����������")]
        [StringLength(255)]
        public string Chief { get; set; }

        [Display(Name = "����� ��������� ����������")]
        [StringLength(255)]
        public string PhoneChief { get; set; }

        [Display(Name = "�������� ����������")]
        [StringLength(255)]
        public string Owner { get; set; }

        [Display(Name = "����� ��������� ����������")]
        [StringLength(255)]
        public string PhoneOwner { get; set; }

        [Display(Name = "����� �������� ����������")]
        [StringLength(255)]
        public string Phone { get; set; }

        [Display(Name = "�����, ��������������")]
        [StringLength(255)]
        public string District { get; set; }

        [Display(Name = "���� ����������")]
        [StringLength(255)]
        public string BankCinema { get; set; }

        [Display(Name = "����� ����������� ����� ����������")]
        public int? AccountCinema { get; set; }

        [Display(Name = "���������� ���������� ����")]
        public int? Capacity { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Rent> Rents { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            Cinema other = (Cinema)obj;
            return Id == other.Id;
        }
    }
}
