using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ShirkunovISP31Cinema
{

    [Table("Rent")]
    public partial class Rent : BaseEntity
    {
        [Display(Name = "����� ������")]
        [Column(TypeName = "date")]
        public DateTime? DateStart { get; set; }

        [Display(Name = "��������� ������")]
        [Column(TypeName = "date")]
        public DateTime? DateStop { get; set; }

        [Display(Name = "������������� ����")]
        [StringLength(255)]
        public string Worker { get; set; }

        [Display(Name = "����� �������� ��")]
        [StringLength(255)]
        public string PhoneWorker { get; set; }

        [Display(Name = "���������")]
        [Column(TypeName = "money")]
        public decimal? Summa { get; set; }

        [Display(Name = "����(������������ ������)")]
        [Column(TypeName = "money")]
        public decimal? Tax { get; set; }

        public int? IdCinema { get; set; }

        [Display(Name = "���������")]
        public string CinemaName => Cinema?.NameCinema;

        public int? IdFilm { get; set; }

        [Display(Name = "�����")]
        public string FilmName => Film?.NameFilm;

        public virtual Cinema Cinema { get; set; }

        public virtual Film Film { get; set; }
    }
}
