﻿using System.ComponentModel.DataAnnotations;

namespace ShirkunovISP31Cinema
{
    /// <summary>
    /// Базовый класс для определения сущностей в приложении
    /// </summary>
    public abstract class BaseEntity
    {
        #region Свойства

        /// <summary>
        /// ID сущности
        /// </summary>
        [Key]
        public int? Id { get; set; }

        #endregion
    }
}
