using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ShirkunovISP31Cinema
{

    [Table("Provider")]
    public partial class Provider : BaseEntity
    {
        public Provider()
        {
            Purchases = new HashSet<Purchase>();
        }

        [Display(Name = "���")]
        [StringLength(255)]
        public string NameProvider { get; set; }

        [Display(Name = "���")]
        public int? INN { get; set; }

        [Display(Name = "�����")]
        [StringLength(255)]
        public string Address { get; set; }

        [Display(Name = "�������� �����")]
        [StringLength(255)]
        public string Bank { get; set; }

        [Display(Name = "����� ����� � �����")]
        public int? Account { get; set; }

        [Display(Name = "������ ����������")]
        public bool? Sign { get; set; }

        public virtual ICollection<Purchase> Purchases { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            Provider other = (Provider)obj;
            return Id == other.Id;
        }
    }
}
