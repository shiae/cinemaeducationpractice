using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ShirkunovISP31Cinema
{

    [Table("Purchase")]
    public partial class Purchase : BaseEntity
    {
        public int? IdProvider { get; set; }

        [Display(Name = "���������")]
        public string ProviderName => Provider?.NameProvider;

        public int? IdFilm { get; set; }

        [Display(Name = "�����")]
        public string FilmName => Film?.NameFilm;

        [Display(Name = "���� �������")]
        [Column(TypeName = "date")]
        public DateTime? DateBuy { get; set; }

        [Display(Name = "����� �������")]
        [Column(TypeName = "money")]
        public decimal? Summ { get; set; }

        public virtual Film Film { get; set; }

        public virtual Provider Provider { get; set; }
    }
}
