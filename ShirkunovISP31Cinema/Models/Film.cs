using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ShirkunovISP31Cinema
{
    [Table("Film")]
    public partial class Film : BaseEntity
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Film()
        {
            Purchases = new HashSet<Purchase>();
            Rents = new HashSet<Rent>();
        }

        [Display(Name = "�������� ������")]
        [StringLength(255)]
        public string NameFilm { get; set; }

        [Display(Name = "����� ��������, ��������")]
        [StringLength(255)]
        public string Author { get; set; }

        [Display(Name = "�����������, ��������")]
        [StringLength(255)]
        public string Comment { get; set; }

        [Display(Name = "��������")]
        [StringLength(255)]
        public string Producer { get; set; }

        [Display(Name = "��������, ������������")]
        [StringLength(255)]
        public string Company { get; set; }

        [Display(Name = "��� �������")]
        public int? Year { get; set; }

        [Display(Name = "������ �������")]
        [StringLength(255)]
        public string Land { get; set; }

        [Display(Name = "�����, � �������")]
        public int? Time { get; set; }

        [Display(Name = "���������")]
        public int? Cost { get; set; }

        [Display(Name = "������� ���������")]
        public int? Rate { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Purchase> Purchases { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Rent> Rents { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            Film other = (Film)obj;
            return Id == other.Id;
        }
    }
}
