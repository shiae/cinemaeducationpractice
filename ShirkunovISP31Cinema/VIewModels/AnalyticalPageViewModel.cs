﻿using iTextSharp.text.pdf;
using iTextSharp.text;
using Microsoft.Win32;
using System.IO;
using System.Windows;
using System;
using System.Windows.Input;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;

namespace ShirkunovISP31Cinema
{
    /// <summary>
    /// Наследуется от <see cref="BaseViewModel"/>
    /// </summary>
    class AnalyticalPageViewModel : BaseViewModel
    {
        #region Приватные поля

        private readonly IFilmService _filmService;
        private readonly IRentService _rentService;

        #endregion

        #region Свойства

        /// <summary>
        /// Название заголовка Отдел маркетинга и закупки
        /// </summary>
        public string MarketingAndPurchasingDepartmentHeader
        {
            get => Get<string>();
            set => Set(value);
        }

        /// <summary>
        /// Название Отдел аренды
        /// </summary>
        public string RentalDepartmentHeader
        {
            get => Get<string>();
            set => Set(value);
        }

        /// <summary>
        /// Название Отчетов
        /// </summary>
        public string ReportsHeader
        {
            get => Get<string>();
            set => Set(value);
        }

        /// <summary>
        /// ViewModel для таблицы с фильмами
        /// </summary>
        public FilmsTableViewModel FilmsTableViewModel
        {
            get => Get<FilmsTableViewModel>();
            set => Set(value);
        }

        /// <summary>
        /// ViewModel для таблицы с покупаками
        /// </summary>
        public PurchaseTableViewModel PurchaseTableViewModel
        {
            get => Get<PurchaseTableViewModel>();
            set => Set(value);
        }

        /// <summary>
        /// ViewModel для таблицы с кинотеатрами
        /// </summary>
        public CinemaTableViewModel CinemaTableViewModel
        {
            get => Get<CinemaTableViewModel>();
            set => Set(value);
        }

        /// <summary>
        /// ViewModel для таблицы с арендами фильмов
        /// </summary>
        public RentTableViewModel RentTableViewModel
        {
            get => Get<RentTableViewModel>();
            set => Set(value);
        }

        /// <summary>
        /// Используется для генерации отчета для рейтинга кинофильмов
        /// </summary>
        public ICommand MovieRatingReportCommand { get; set; }

        /// <summary>
        /// Используется для генерации отчета для долгов за месяц
        /// </summary>
        public ICommand DebtReportCommand { get; set; }

        /// <summary>
        /// Используется для генерации отчета для долгов за месяц
        /// </summary>
        public ICommand MonthRentReportCommand { get; set; }

        /// <summary>
        /// Используется для генерации отчета для долгов за месяц
        /// </summary>
        public ICommand IncomeReportCommand { get; set; }


        #endregion

        #region Конструкторы

        public AnalyticalPageViewModel()
        {
            MarketingAndPurchasingDepartmentHeader = "Отдел маркетинга и закупки";
            RentalDepartmentHeader = "Отдел аренды";
            ReportsHeader = "Отчёты";
            FilmsTableViewModel = new FilmsTableViewModel();
            PurchaseTableViewModel = new PurchaseTableViewModel();
            CinemaTableViewModel = new CinemaTableViewModel();
            RentTableViewModel = new RentTableViewModel();
            MovieRatingReportCommand = new RelayCommand(() => MovieRatingReport());
            DebtReportCommand = new RelayCommand(() => DebtReport());
            MonthRentReportCommand = new RelayCommand(() => MonthRentReport());
            IncomeReportCommand = new RelayCommand(() => IncomeReport());
            _filmService = new FilmService();
            _rentService = new RentService();
        }

        #endregion

        #region Методы

        public void IncomeReport()
        {
            IncomeWindow incomeWindow = new IncomeWindow();
            incomeWindow.ShowDialog();
        }

        public void MovieRatingReport()
        {
            try
            {
                // Создание документа
                Document document = new Document();
                string fontPath = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Fonts), "ARIAL.TTF");
                BaseFont baseFont = BaseFont.CreateFont(fontPath, BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
                Font font = new Font(baseFont, 14f, Font.NORMAL);

                // Открытие диалогового окна для выбора пути сохранения файла
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "PDF Files (*.pdf)|*.pdf";
                saveFileDialog.DefaultExt = "pdf";
                if (saveFileDialog.ShowDialog() == true)
                {
                    string filename = saveFileDialog.FileName;

                    // Создание PDF writer для записи в файл
                    PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(filename, FileMode.Create));

                    // Открытие документа
                    document.Open();

                    // Создание заголовка
                    Paragraph title = new Paragraph("Рейтинг фильмов", font);
                    title.Alignment = Element.ALIGN_CENTER;
                    title.SpacingAfter = 10;
                    document.Add(title);

                    // Создание таблицы
                    PdfPTable table = new PdfPTable(3);
                    table.WidthPercentage = 100;

                    // Добавление заголовков столбцов
                    table.AddCell(new PdfPCell(new Phrase("Название", font)));
                    table.AddCell(new PdfPCell(new Phrase("Год", font)));
                    table.AddCell(new PdfPCell(new Phrase("Рейтинг", font)));

                    // Получение данных из базы данных
                    var films = _filmService.FindAll();

                    // Добавление данных фильмов в таблицу
                    foreach (var film in films)
                    {
                        table.AddCell(new PdfPCell(new Phrase(film.NameFilm, font)));
                        table.AddCell(new PdfPCell(new Phrase(film.Year.ToString(), font)));
                        table.AddCell(new PdfPCell(new Phrase(film.Rate.ToString(), font)));
                    }

                    // Добавление таблицы в документ
                    document.Add(table);

                    // Закрытие документа
                    document.Close();

                }
            }
            catch
            {
                MessageBox.Show("Не удалось создать отчет о рейтинге!", "Ошибка");
            }
        }

        public void DebtReport()
        {
            try
            {
                // Получение текущего месяца и года
                int currentMonth = DateTime.Now.Month;
                int currentYear = DateTime.Now.Year;

                // Фильтрация записей аренды по долгам в текущем месяце
                var debts = _rentService.FindAll()
                    .ToList()
                    .Where(r => r.Tax > 0 && r.DateStart.Value.Month == currentMonth && r.DateStart.Value.Year == currentYear)
                    .Select(r => new
                    {
                        IdRent = r.Id,
                        Worker = r.Worker,
                        PhoneWorker = r.PhoneWorker,
                        Summa = r.Summa,
                        Tax = r.Tax
                    })
                    .ToList();

                // Создание документа
                Document document = new Document();
                string fontPath = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Fonts), "ARIAL.TTF");
                BaseFont baseFont = BaseFont.CreateFont(fontPath, BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
                Font font = new Font(baseFont, 14f, Font.NORMAL);

                // Открытие диалогового окна для выбора пути сохранения файла
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "PDF Files (*.pdf)|*.pdf";
                saveFileDialog.DefaultExt = "pdf";
                if (saveFileDialog.ShowDialog() == true)
                {
                    string filename = saveFileDialog.FileName;

                    // Создание PDF writer для записи в файл
                    PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(filename, FileMode.Create));

                    // Открытие документа
                    document.Open();

                    // Создание заголовка
                    Paragraph title = new Paragraph("Отчет по долгам за месяц", font);
                    title.Alignment = Element.ALIGN_CENTER;
                    title.SpacingAfter = 10;
                    document.Add(title);

                    // Создание таблицы
                    PdfPTable table = new PdfPTable(5);
                    table.WidthPercentage = 100;

                    // Добавление заголовков столбцов
                    table.AddCell(new PdfPCell(new Phrase("Id договора", font)));
                    table.AddCell(new PdfPCell(new Phrase("Ответственное лицо", font)));
                    table.AddCell(new PdfPCell(new Phrase("Телефон ОЛ", font)));
                    table.AddCell(new PdfPCell(new Phrase("Сумма", font)));
                    table.AddCell(new PdfPCell(new Phrase("Пени", font)));

                    // Добавление данных о долгах в таблицу
                    foreach (var debt in debts)
                    {
                        table.AddCell(new PdfPCell(new Phrase(debt.IdRent.ToString(), font)));
                        table.AddCell(new PdfPCell(new Phrase(debt.Worker, font)));
                        table.AddCell(new PdfPCell(new Phrase(debt.PhoneWorker, font)));
                        table.AddCell(new PdfPCell(new Phrase(debt.Summa.ToString(), font)));
                        table.AddCell(new PdfPCell(new Phrase(debt.Tax.ToString(), font)));
                    }

                    // Добавление таблицы в документ
                    document.Add(table);

                    // Закрытие документа
                    document.Close();
                }
            }
            catch
            {
                MessageBox.Show("Не удалось создать отчет о долгах!", "Ошибка");
            }
        }

        public void MonthRentReport()
        {
            try
            {
                // Создание документа
                Document document = new Document();
                string fontPath = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Fonts), "ARIAL.TTF");
                BaseFont baseFont = BaseFont.CreateFont(fontPath, BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
                Font font = new Font(baseFont, 14f, Font.NORMAL);

                // Открытие диалогового окна для выбора пути сохранения файла
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "PDF Files (*.pdf)|*.pdf";
                saveFileDialog.DefaultExt = "pdf";
                if (saveFileDialog.ShowDialog() == true)
                {
                    string filename = saveFileDialog.FileName;

                    // Создание PDF writer для записи в файл
                    PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(filename, FileMode.Create));

                    // Открытие документа
                    document.Open();

                    // Создание заголовка
                    Paragraph title = new Paragraph("Отчет об аренде за прошлый месяц", font);
                    title.Alignment = Element.ALIGN_CENTER;
                    title.SpacingAfter = 10;
                    document.Add(title);

                    // Создание таблицы
                    PdfPTable table = new PdfPTable(9);
                    table.WidthPercentage = 100;

                    // Добавление заголовков столбцов
                    table.AddCell(new PdfPCell(new Phrase("Id аренды", font)));
                    table.AddCell(new PdfPCell(new Phrase("Начало", font)));
                    table.AddCell(new PdfPCell(new Phrase("Окончание", font)));
                    table.AddCell(new PdfPCell(new Phrase("Ответственное лицо", font)));
                    table.AddCell(new PdfPCell(new Phrase("Телефон ОЛ", font)));
                    table.AddCell(new PdfPCell(new Phrase("Сумма", font)));
                    table.AddCell(new PdfPCell(new Phrase("Пени", font)));
                    table.AddCell(new PdfPCell(new Phrase("Кинотеатр", font)));
                    table.AddCell(new PdfPCell(new Phrase("Фильм", font)));

                    // Получение данных из базы данных
                    DateTime currentMonth = DateTime.Now;
                    DateTime previousMonth = currentMonth.AddMonths(-1);

                    var rentals = _rentService.FindAll().ToList().Where(r => r.DateStart.Value.Month == previousMonth.Month && r.DateStart.Value.Year == previousMonth.Year).ToList();

                    // Добавление данных аренды в таблицу
                    foreach (var rental in rentals)
                    {
                        table.AddCell(new PdfPCell(new Phrase(rental.Id.ToString(), font)));
                        table.AddCell(new PdfPCell(new Phrase(rental.DateStart.ToString(), font)));
                        table.AddCell(new PdfPCell(new Phrase(rental.DateStop.ToString(), font)));
                        table.AddCell(new PdfPCell(new Phrase(rental.Worker, font)));
                        table.AddCell(new PdfPCell(new Phrase(rental.PhoneWorker, font)));
                        table.AddCell(new PdfPCell(new Phrase(rental.Summa.ToString(), font)));
                        table.AddCell(new PdfPCell(new Phrase(rental.Tax.ToString(), font)));
                        table.AddCell(new PdfPCell(new Phrase(rental?.Cinema?.NameCinema, font)));
                        table.AddCell(new PdfPCell(new Phrase(rental?.Film?.NameFilm, font)));
                    }

                    // Добавление таблицы в документ
                    document.Add(table);

                    // Закрытие документа
                    document.Close();
                }
            }
            catch
            {
                MessageBox.Show("Не удалось создать отчет об аренде!", "Ошибка");
            }
        }

        #endregion
    }
}
