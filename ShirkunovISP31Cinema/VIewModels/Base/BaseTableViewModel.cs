﻿using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace ShirkunovISP31Cinema
{
    /// <summary>
    /// Базовый ViewModel для таблиц
    /// </summary>
    /// <typeparam name="Entity">Тип сущности, которая будет регулировать в таблице</typeparam>
    public abstract class BaseTableViewModel<Entity> : BaseViewModel
        where Entity : BaseEntity
    {
        #region Свойства

        /// <summary>
        /// Заголовок таблицы
        /// </summary>
        public string Title
        {
            get => Get<string>();
            set => Set(value);
        }

        /// <summary>
        /// Путь к картинке таблицы
        /// </summary>
        public string TitleImagePath
        {
            get => Get<string>();
            set => Set(value);
        }

        #endregion

        #region Абстрактные Свойства

        /// <summary>
        /// Строки таблицы
        /// </summary>
        public abstract ObservableCollection<Entity> Data { get; set; }

        #endregion

        #region Команды

        /// <summary>
        /// Используется для выполнения добавления элемента в таблицу
        /// </summary>
        public ICommand AddItemCommand { get; set; }

        /// <summary>
        /// Используется для изменения элемента в таблице
        /// </summary>
        public ICommand EditItemCommand { get; set; }

        /// <summary>
        /// Используется для выполнения удаления из таблицы
        /// </summary>
        public ICommand DeleteItemCommand { get; set; }

        /// <summary>
        /// Используется для просмотра сущности
        /// </summary>
        public ICommand OpenCommand { get; set; }

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public BaseTableViewModel(string title, string titleImagePath)
        {
            AddItemCommand = new RelayCommand(() => AddItem());
            DeleteItemCommand = new RelayParameterizedCommand((entity) => Delete((Entity)entity));
            EditItemCommand = new RelayParameterizedCommand((entity) => EditItem((Entity)entity));
            OpenCommand = new RelayParameterizedCommand((entity) => OpenItem((Entity)entity));
            Title = title;
            TitleImagePath = titleImagePath;
        }

        #endregion

        #region Абстрактные методы

        /// <summary>
        /// Используется для добавления нового элемента в таблицу
        /// </summary>
        protected abstract void AddItem();

        /// <summary>
        /// Используется для изменения существующего элемента в таблице
        /// </summary>
        protected abstract void EditItem(Entity entity);

        /// <summary>
        /// Используется для удаления сущности по ID
        /// </summary>
        /// <param name="id">ID сущности</param>
        protected abstract void DeleteItemById(int id);

        /// <summary>
        /// Используется для просмотра сущности
        /// </summary>
        protected abstract void OpenItem(Entity entity);

        #endregion

        #region Методы

        /// <summary>
        /// Используется для удаления элемента в гриде
        /// </summary>
        /// <param name="parameter">Таблица</param>
        public void Delete(Entity entity)
        {
            if (entity != null)
            {
                MessageBoxResult result = MessageBox.Show(
                    "Вы уверены, что хотите удалить запись?",
                    "Удаление данных",
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Information
                );

                if (result == MessageBoxResult.Yes)
                {
                    Entity foundEntity = Data.Where(item => item.Id == entity.Id).FirstOrDefault();

                    if (foundEntity != null)
                    {
                        Data.Remove(foundEntity);
                        DeleteItemById((int)entity.Id);
                    }
                }
            }
        }

        #endregion
    }
}
