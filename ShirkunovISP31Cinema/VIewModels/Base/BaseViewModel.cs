﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;

namespace ShirkunovISP31Cinema
{
    public abstract class BaseViewModel : INotifyPropertyChanged
    {
        #region Приватные поля

        /// <summary>
        /// Хранит в себе значения всех свойств конкретной реализации <see cref="BaseViewModel" />
        /// </summary>
        private readonly Dictionary<string, object> _propertyValues = new Dictionary<string, object>();

        #endregion

        #region Свойства

        /// <summary>
        /// Вызывается при любом изменении состояния элемента на пользовательском интерфейсе
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged = (sender, e) => { };

        #endregion

        #region Методы

        /// <summary>
        /// Используется для получения свойства из словаря свойств <see cref="_propertyValues"/>
        /// </summary>
        /// <typeparam name="T">Тип свойства</typeparam>
        /// <param name="propertyName">Название свойства</param>
        /// <returns>Значение свойства</returns>
        protected T Get<T>([CallerMemberName] string propertyName = null)
        {
            if (_propertyValues.TryGetValue(propertyName, out object value))
            {
                return (T)value;
            }

            return default(T);
        }

        /// <summary>
        /// Используеться для добавления свойства в словарь свойств <see cref="_propertyValues"/>
        /// </summary>
        /// <typeparam name="T">Тип значения свойства</typeparam>
        /// <param name="value">Значение свойства</param>
        /// <param name="propertyName">Название свойства</param>
        protected void Set<T>(T value, [CallerMemberName] string propertyName = null)
        {
            if (!EqualityComparer<T>.Default.Equals(Get<T>(propertyName), value))
            {
                _propertyValues[propertyName] = value;
                OnPropertyChanged(propertyName);
            }
        }

        /// <summary>
        /// Вызывается для вызова <see cref="PropertyChanged"/> события
        /// </summary>
        /// <param name="name"></param>
        public void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        /// <summary>
        /// Используется для вывода пользователю сообщения об ошибке
        /// </summary>
        /// <param name="message">Сообщение</param>
        public void ShowErrorMessage(string message)
        {
            MessageBox.Show(
                message,
                "Ошибка",
                MessageBoxButton.OK,
                MessageBoxImage.Error
           );
        }

        #endregion

    }
}
