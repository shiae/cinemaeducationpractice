﻿using iTextSharp.text.pdf;
using iTextSharp.text;
using Microsoft.Win32;
using System;
using System.IO;
using System.Windows;
using System.Windows.Input;
using System.Linq;

namespace ShirkunovISP31Cinema
{
    public class IncomeWindowViewModel : BaseViewModel
    {
        #region Приватные поля

        private IRentService _rentService;
        private IncomeWindow _incomeWindow;

        #endregion

        #region Свойства

        /// <summary>
        /// Используется для генерации отчета
        /// </summary>
        public ICommand IncomeReportCommand { get; set; }

        #endregion

        #region Конструкторы

        public IncomeWindowViewModel(IncomeWindow incomeWindow)
        {
            IncomeReportCommand = new RelayParameterizedCommand(sender => IncomeReport(sender));
            _rentService = new RentService();
            _incomeWindow = incomeWindow;
        }

        #endregion

        #region Методы

        public void IncomeReport(object sender)
        {
            if (sender is IncomeWindow incomeWindow)
            {
                if (DateTime.TryParse(incomeWindow.tbStartDate.Text, out DateTime startDate) &&
                    DateTime.TryParse(incomeWindow.tbEndDate.Text, out DateTime endDate))
                {
                    try
                    {
                        // Форматирование дат в заголовке
                        string reportTitle = $"Доход за период {startDate.ToShortDateString()} - {endDate.ToShortDateString()}";

                        // Создание документа
                        Document document = new Document();
                        string fontPath = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Fonts), "ARIAL.TTF");
                        BaseFont baseFont = BaseFont.CreateFont(fontPath, BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
                        Font font = new Font(baseFont, 14f, Font.NORMAL);

                        // Открытие диалогового окна для выбора пути сохранения файла
                        SaveFileDialog saveFileDialog = new SaveFileDialog();
                        saveFileDialog.Filter = "PDF Files (*.pdf)|*.pdf";
                        saveFileDialog.DefaultExt = "pdf";
                        if (saveFileDialog.ShowDialog() == true)
                        {
                            string filename = saveFileDialog.FileName;

                            // Создание PDF writer для записи в файл
                            PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(filename, FileMode.Create));

                            // Открытие документа
                            document.Open();

                            // Добавление заголовка
                            Paragraph title = new Paragraph(reportTitle, font);
                            title.Alignment = Element.ALIGN_CENTER;
                            title.SpacingAfter = 10;
                            document.Add(title);

                            // Создание таблицы
                            PdfPTable table = new PdfPTable(2);
                            table.WidthPercentage = 100;

                            // Добавление заголовков столбцов
                            table.AddCell(new PdfPCell(new Phrase("Дата", font)));
                            table.AddCell(new PdfPCell(new Phrase("Выручка", font)));

                            // Получение данных из базы данных
                            var rentals = _rentService.FindAll().ToList().Where(r => r.DateStart >= startDate && r.DateStop <= endDate && r.Tax > 0).ToList();

                            // Добавление данных аренды в таблицу
                            decimal totalIncome = 0;
                            foreach (var rental in rentals)
                            {
                                table.AddCell(new PdfPCell(new Phrase(rental.DateStart.ToString(), font)));
                                table.AddCell(new PdfPCell(new Phrase(rental.Summa.ToString(), font)));

                                totalIncome += rental.Summa.Value;
                            }

                            // Добавление таблицы в документ
                            document.Add(table);

                            // Добавление заголовка с итоговой суммой
                            Paragraph totalIncomeTitle = new Paragraph($"Итого {totalIncome}", font);
                            totalIncomeTitle.Alignment = Element.ALIGN_RIGHT;
                            totalIncomeTitle.SpacingBefore = 10;
                            document.Add(totalIncomeTitle);

                            // Закрытие документа
                            document.Close();

                            MessageBox.Show("Отчёт был успешно создан", "Уведомление");
                            _incomeWindow.Close();
                        }
                    }
                    catch
                    {
                        MessageBox.Show("Не удалось создать отчет о доходах!", "Ошибка");
                    }
                }
                else
                {
                    ShowErrorMessage("Был введен некорректный период! Проверьте правильность заполнения дат, формат: yyyy.mm.dd");
                }
            }
        }

        #endregion
    }
}
