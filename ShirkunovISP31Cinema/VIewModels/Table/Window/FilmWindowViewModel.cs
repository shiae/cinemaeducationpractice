﻿namespace ShirkunovISP31Cinema
{
    public class FilmWindowViewModel : BaseWindowViewModel<Film>
    {
        #region Свойства

        public string NameFilm
        {
            get => Get<string>();
            set
            {
                Set(value);
                EditedField = true;
            }
        }

        public string Author
        {
            get => Get<string>();
            set
            {
                Set(value);
                EditedField = true;
            }
        }

        public string Comment
        {
            get => Get<string>();
            set
            {
                Set(value);
                EditedField = true;
            }
        }

        public string Producer
        {
            get => Get<string>();
            set
            {
                Set(value);
                EditedField = true;
            }
        }

        public string Company
        {
            get => Get<string>();
            set
            {
                Set(value);
                EditedField = true;
            }
        }

        public int? Year
        {
            get => Get<int?>();
            set
            {
                Set(value);
                EditedField = true;
            }
        }

        public string Land
        {
            get => Get<string>();
            set
            {
                Set(value);
                EditedField = true;
            }
        }

        public int? Time
        {
            get => Get<int?>();
            set
            {
                Set(value);
                EditedField = true;
            }
        }

        public int? Cost
        {
            get => Get<int?>();
            set
            {
                Set(value);
                EditedField = true;
            }
        }

        public int? Rate
        {
            get => Get<int?>();
            set
            {
                Set(value);
                EditedField = true;
            }
        }

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        /// <param name="film">Фильм</param>
        /// <param name="isEditing">True - доступен для редактирования</param>
        public FilmWindowViewModel(Film film, OpeningRegime openingRegime) : base(film, openingRegime)
        {
            NameFilm = film.NameFilm;
            Author = film.Author;
            Comment = film.Comment;
            Producer = film.Producer;
            Company = film.Company;
            Year = film.Year;
            Land = film.Land;
            Time = film.Time;
            Cost = film.Cost;
            Rate = film.Rate;
            EditedField = false;
        }

        #endregion

        #region Методы BaseWindowViewModel

        public override void OnSave(object parameter)
        {
            Film currentFilm = new Film();
            currentFilm.NameFilm = NameFilm;
            currentFilm.Author = Author;
            currentFilm.Producer = Producer;
            currentFilm.Company = Company;
            currentFilm.Year = Year;
            currentFilm.Land = Land;
            currentFilm.Time = Time;
            currentFilm.Cost = Cost;
            currentFilm.Rate = Rate;
            currentFilm.Comment = Comment;

            Save(currentFilm, parameter);
        }

        #endregion
    }
}
