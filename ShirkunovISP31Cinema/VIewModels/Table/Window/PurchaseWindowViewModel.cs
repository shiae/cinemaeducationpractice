﻿using System;
using System.Collections.ObjectModel;
using System.Windows;

namespace ShirkunovISP31Cinema
{
    public class PurchaseWindowViewModel : BaseWindowViewModel<Purchase>
    {
        #region Свойства

        public string ProviderName
        {
            get => Get<string>();
            set => Set(value);
        }

        public string FilmName
        {
            get => Get<string>();
            set => Set(value);
        }

        public string DateBuy
        {
            get => Get<string>();
            set => Set(value);
        }

        public decimal? Summ
        {
            get => Get<decimal?>();
            set => Set(value);
        }

        public Film Film
        {
            get => Get<Film>();
            set => Set(value);
        }

        public ObservableCollection<Film> Films
        {
            get => Get<ObservableCollection<Film>>();
            set => Set(value);
        }

        public Provider Provider
        {
            get => Get<Provider>();
            set => Set(value);
        }

        public ObservableCollection<Provider> Providers
        {
            get => Get<ObservableCollection<Provider>>();
            set => Set(value);
        }

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        /// <param name="purchase">Покупки</param>
        /// <param name="isEditing">True - доступен для редактирования</param>
        public PurchaseWindowViewModel(Purchase purchase, OpeningRegime openingRegime) : base(purchase, openingRegime)
        {
            DateBuy = purchase.DateBuy.ToString();
            Summ = purchase.Summ;
            ProviderName = purchase?.Provider?.NameProvider;
            FilmName = purchase?.Film?.NameFilm;
            Film = purchase.Film;
            Provider = purchase.Provider;
            EditedField = false;

            FilmService filmService = new FilmService();
            Films = new ObservableCollection<Film>(filmService.FindAll());

            ProviderService providerService = new ProviderService();
            Providers = new ObservableCollection<Provider>(providerService.FindAll());
        }

        #endregion

        #region Методы BaseWindowViewModel

        public override void OnSave(object parameter)
        {
            Purchase currentPurchase = new Purchase();
            currentPurchase.Summ = Summ;
            currentPurchase.IdProvider = Provider?.Id;
            currentPurchase.IdFilm = Film?.Id;

            DateTime date;

            if (DateTime.TryParse(DateBuy, out date))
            {
                currentPurchase.DateBuy = date;
            }
            else
            {
                MessageBox.Show("Неверный формат даты. Введите дату формата dd/MM/yyyy, где dd - день, MM - месяц и yyyy - год", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            Save(currentPurchase, parameter);
        }

        #endregion
    }
}
