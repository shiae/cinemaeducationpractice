﻿namespace ShirkunovISP31Cinema
{
    public class CinemaWindowViewModel : BaseWindowViewModel<Cinema>
    {
        #region Свойства

        public string NameCinema
        {
            get => Get<string>();
            set => Set(value);
        }

        public int? INNCinema
        {
            get => Get<int>();
            set => Set(value);
        }

        public string Address
        {
            get => Get<string>();
            set => Set(value);
        }

        public string Chief
        {
            get => Get<string>();
            set => Set(value);
        }

        public string PhoneChief
        {
            get => Get<string>();
            set => Set(value);
        }

        public string Owner
        {
            get => Get<string>();
            set => Set(value);
        }

        public string PhoneOwner
        {
            get => Get<string>();
            set => Set(value);
        }

        public string Phone
        {
            get => Get<string>();
            set => Set(value);
        }

        public string District
        {
            get => Get<string>();
            set => Set(value);
        }

        public string BankCinema
        {
            get => Get<string>();
            set => Set(value);
        }

        public int? AccountCinema
        {
            get => Get<int>();
            set => Set(value);
        }

        public int? Capacity
        {
            get => Get<int>();
            set => Set(value);
        }

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        /// <param name="film">Фильм</param>
        /// <param name="isEditing">True - доступен для редактирования</param>
        public CinemaWindowViewModel(Cinema cinema, OpeningRegime openingRegime) : base(cinema, openingRegime)
        {
            NameCinema = cinema.NameCinema;
            INNCinema = cinema.INNCinema;
            Address = cinema.Address;
            Chief = cinema.Chief;
            PhoneChief = cinema.PhoneChief;
            Owner = cinema.Owner;
            PhoneOwner = cinema.PhoneOwner;
            Phone = cinema.Phone;
            District = cinema.District;
            BankCinema = cinema.BankCinema;
            AccountCinema = cinema.AccountCinema;
            Capacity = cinema.Capacity;
            EditedField = false;
        }

        #endregion

        #region Методы BaseWindowViewModel

        public override void OnSave(object parameter)
        {
            Cinema cinema = new Cinema();
            cinema.NameCinema = NameCinema;
            cinema.INNCinema = INNCinema;
            cinema.Address = Address;
            cinema.Chief = Chief;
            cinema.PhoneChief = PhoneChief;
            cinema.Owner = Owner;
            cinema.PhoneOwner = PhoneOwner;
            cinema.Phone = Phone;
            cinema.District = District;
            cinema.BankCinema = BankCinema;
            cinema.AccountCinema = AccountCinema;
            cinema.Capacity = Capacity;

            Save(cinema, parameter);
        }

        #endregion
    }
}