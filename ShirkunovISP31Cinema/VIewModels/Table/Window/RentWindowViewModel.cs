﻿using System;
using System.Collections.ObjectModel;
using System.Windows;

namespace ShirkunovISP31Cinema
{
    public class RentWindowViewModel : BaseWindowViewModel<Rent>
    {
        #region Свойства

        public string DateStart
        {
            get => Get<string>();
            set => Set(value);
        }

        public string DateStop
        {
            get => Get<string>();
            set => Set(value);
        }

        public string Worker
        {
            get => Get<string>();
            set => Set(value);
        }

        public string PhoneWorker
        {
            get => Get<string>();
            set => Set(value);
        }

        public decimal? Summa
        {
            get => Get<decimal?>();
            set => Set(value);
        }

        public decimal? Tax
        {
            get => Get<decimal?>();
            set => Set(value);
        }

        public Cinema Cinema
        {
            get => Get<Cinema>();
            set => Set(value);
        }

        public Film Film
        {
            get => Get<Film>();
            set => Set(value);
        }

        public ObservableCollection<Cinema> Cinemas
        {
            get => Get<ObservableCollection<Cinema>>();
            set => Set(value);
        }

        public ObservableCollection<Film> Films
        {
            get => Get<ObservableCollection<Film>>();
            set => Set(value);
        }

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        /// <param name="film">Фильм</param>
        /// <param name="isEditing">True - доступен для редактирования</param>
        public RentWindowViewModel(Rent rent, OpeningRegime openingRegime) : base(rent, openingRegime)
        {
            DateStart = rent.DateStart.ToString();
            DateStop = rent.DateStop.ToString();
            Worker = rent.Worker;
            PhoneWorker = rent.PhoneWorker;
            Summa = rent.Summa;
            Tax = rent.Tax;
            Cinema = rent.Cinema;
            Film = rent.Film;
            EditedField = false;

            CinemaService cinemaService = new CinemaService();
            Cinemas = new ObservableCollection<Cinema>(cinemaService.FindAll());

            FilmService filmService = new FilmService();
            Films = new ObservableCollection<Film>(filmService.FindAll());
        }

        #endregion

        #region Методы BaseWindowViewModel

        public override void OnSave(object parameter)
        {
            Rent currentRent = new Rent();

            DateTime date;

            if (DateTime.TryParse(DateStart, out date))
            {
                currentRent.DateStart = date;
            }
            else
            {
                MessageBox.Show("Неверный формат даты. Введите дату формата dd/MM/yyyy, где dd - день, MM - месяц и yyyy - год", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (DateTime.TryParse(DateStop, out date))
            {
                currentRent.DateStop = date;
            }
            else
            {
                MessageBox.Show("Неверный формат даты. Введите дату формата dd/MM/yyyy, где dd - день, MM - месяц и yyyy - год", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            currentRent.Worker = Worker;
            currentRent.PhoneWorker = PhoneWorker;
            currentRent.Summa = Summa;
            currentRent.Tax = Tax;
            currentRent.IdCinema = Cinema?.Id;
            currentRent.IdFilm = Film?.Id;

            Save(currentRent, parameter);
        }

        #endregion
    }
}
