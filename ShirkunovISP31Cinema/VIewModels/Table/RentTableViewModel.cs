﻿using System.Collections.ObjectModel;

namespace ShirkunovISP31Cinema
{
    public class RentTableViewModel : BaseTableViewModel<Rent>
    {
        #region Поля

        /// <summary>
        /// Бизнес - логика для работы с фильмами
        /// </summary>
        private readonly IRentService _rentService;

        #endregion

        #region Свойства BaseTableViewModel

        /// <summary>
        /// Данные таблицы
        /// </summary>
        public override ObservableCollection<Rent> Data
        {
            get => Get<ObservableCollection<Rent>>();
            set => Set(value);
        }

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        /// <param name="title">Заголовок таблицы</param>
        /// <param name="titleImagePath">Путь к заголовку таблицы</param>
        public RentTableViewModel() : base("Аренда фильмов", "pack://application:,,,/Images/Tables/calendar.png")
        {
            _rentService = new RentService();
            Data = new ObservableCollection<Rent>(_rentService.FindAll());
        }

        #endregion

        #region Методы BaseTableViewModel

        // <summary>
        /// Используется для добавления нового элемента в таблицу
        /// </summary>
        protected override void AddItem()
        {
            RentWindow rentWindow = new RentWindow();
            rentWindow.OpenForAdd();

            if (rentWindow.DialogResult == true && rentWindow.DataContext != null && rentWindow.DataContext is RentWindowViewModel rentWindowViewModel)
            {
                Rent newRent = rentWindowViewModel.OriginalEntity;
                Rent savedRent = _rentService.Save(newRent);
                Data.Add(savedRent);
            }
        }

        /// <summary>
        /// Используется для изменения существующего элемента в таблице
        /// </summary>
        protected override void EditItem(Rent entity)
        {
            RentWindow rentWindow = new RentWindow();
            rentWindow.OpenForEdit(entity);

            if (rentWindow.DialogResult == true && rentWindow.DataContext != null && rentWindow.DataContext is RentWindowViewModel rentWindowViewModel)
            {
                Rent newRent = rentWindowViewModel.OriginalEntity;
                _rentService.Update(newRent);
                Data = new ObservableCollection<Rent>(_rentService.FindAll());
            }
        }

        /// <summary>
        /// Используется для удаления элемента в гриде
        /// </summary>
        /// <param name="parameter">Таблица</param>
        protected override void DeleteItemById(int id)
        {
            _rentService.DeleteById(id);
        }

        /// <summary>
        /// Используется для просмотра сущности
        /// </summary>
        protected override void OpenItem(Rent entity)
        {
            RentWindow rentWindow = new RentWindow();
            rentWindow.OpenForView(entity);
        }

        #endregion
    }
}
