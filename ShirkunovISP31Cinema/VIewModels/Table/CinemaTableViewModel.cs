﻿using System.Collections.ObjectModel;

namespace ShirkunovISP31Cinema
{
    public class CinemaTableViewModel : BaseTableViewModel<Cinema>
    {
        #region Поля

        /// <summary>
        /// Бизнес - логика для работы с фильмами
        /// </summary>
        private readonly ICinemaService _cinemaService;

        #endregion

        #region Свойства BaseTableViewModel

        /// <summary>
        /// Данные таблицы
        /// </summary>
        public override ObservableCollection<Cinema> Data
        {
            get => Get<ObservableCollection<Cinema>>();
            set => Set(value);
        }

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        /// <param name="title">Заголовок таблицы</param>
        /// <param name="titleImagePath">Путь к заголовку таблицы</param>
        public CinemaTableViewModel() : base("Кинотеатры", "pack://application:,,,/Images/Tables/ticket.png")
        {
            _cinemaService = new CinemaService();
            Data = new ObservableCollection<Cinema>(_cinemaService.FindAll());
        }

        #endregion

        #region Методы BaseTableViewModel

        /// <summary>
        /// Используется для добавления нового элемента в таблицу
        /// </summary>
        protected override void AddItem()
        {
            CinemaWindow cinemaWindow = new CinemaWindow();
            cinemaWindow.OpenForAdd();

            if (cinemaWindow.DialogResult == true && cinemaWindow.DataContext != null && cinemaWindow.DataContext is CinemaWindowViewModel cinemaWindowViewModel)
            {
                Cinema newCinema = cinemaWindowViewModel.OriginalEntity;
                Cinema savedCinema = _cinemaService.Save(newCinema);
                Data.Add(savedCinema);
            }
        }

        /// <summary>
        /// Используется для изменения существующего элемента в таблице
        /// </summary>
        protected override void EditItem(Cinema entity)
        {
            CinemaWindow filmWindow = new CinemaWindow();
            filmWindow.OpenForEdit(entity);

            if (filmWindow.DialogResult == true && filmWindow.DataContext != null && filmWindow.DataContext is CinemaWindowViewModel cinemaWindowViewModel)
            {
                Cinema newCinema = cinemaWindowViewModel.OriginalEntity;
                _cinemaService.Update(newCinema);
                Data = new ObservableCollection<Cinema>(_cinemaService.FindAll());
            }
        }

        /// <summary>
        /// Используется для удаления элемента в гриде
        /// </summary>
        /// <param name="parameter">Таблица</param>
        protected override void DeleteItemById(int id)
        {
            _cinemaService.DeleteById(id);
        }

        /// <summary>
        /// Используется для просмотра сущности
        /// </summary>
        protected override void OpenItem(Cinema entity)
        {
            CinemaWindow filmWindow = new CinemaWindow();
            filmWindow.OpenForView(entity);
        }

        #endregion
    }
}
