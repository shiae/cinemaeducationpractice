﻿using System;
using System.Collections.ObjectModel;

namespace ShirkunovISP31Cinema
{
    public class PurchaseTableViewModel : BaseTableViewModel<Purchase>
    {
        #region Поля

        /// <summary>
        /// Бизнес - логика для работы с фильмами
        /// </summary>
        private readonly IPurchaseService _purchaseService;

        #endregion

        #region Свойства BaseTableViewModel

        /// <summary>
        /// Данные таблицы
        /// </summary>
        public override ObservableCollection<Purchase> Data
        {
            get => Get<ObservableCollection<Purchase>>();
            set => Set(value);
        }

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        /// <param name="title">Заголовок таблицы</param>
        /// <param name="titleImagePath">Путь к заголовку таблицы</param>
        public PurchaseTableViewModel() : base("Закупка", "pack://application:,,,/Images/Tables/cart.png")
        {
            _purchaseService = new PurchaseService();
            Data = new ObservableCollection<Purchase>(_purchaseService.FindAll());
        }

        #endregion

        #region Методы BaseTableViewModel

        /// <summary>
        /// Используется для добавления нового элемента в таблицу
        /// </summary>
        protected override void AddItem()
        {
            PurchaseWindow purchaseWindow = new PurchaseWindow();
            purchaseWindow.OpenForAdd();

            if (purchaseWindow.DialogResult == true && purchaseWindow.DataContext != null && purchaseWindow.DataContext is PurchaseWindowViewModel purchaseWindowViewModel)
            {
                Purchase newPurchase = purchaseWindowViewModel.OriginalEntity;
                Purchase savedPurchase = _purchaseService.Save(newPurchase);
                Data.Add(savedPurchase);
            }
        }

        /// <summary>
        /// Используется для изменения существующего элемента в таблице
        /// </summary>
        protected override void EditItem(Purchase entity)
        {
            PurchaseWindow purchaseWindow = new PurchaseWindow();
            purchaseWindow.OpenForEdit(entity);

            if (purchaseWindow.DialogResult == true && purchaseWindow.DataContext != null && purchaseWindow.DataContext is PurchaseWindowViewModel purchaseWindowViewModel)
            {
                Purchase newPurchase = purchaseWindowViewModel.OriginalEntity;
                _purchaseService.Update(newPurchase);
                Data = new ObservableCollection<Purchase>(_purchaseService.FindAll());
            }
        }

        /// <summary>
        /// Используется для удаления элемента в гриде
        /// </summary>
        /// <param name="parameter">Таблица</param>
        protected override void DeleteItemById(int id)
        {
            _purchaseService.DeleteById(id);
        }

        /// <summary>
        /// Используется для просмотра сущности
        /// </summary>
        protected override void OpenItem(Purchase entity)
        {
            PurchaseWindow purchaseWindow = new PurchaseWindow();
            purchaseWindow.OpenForView(entity);
        }

        #endregion
    }
}
