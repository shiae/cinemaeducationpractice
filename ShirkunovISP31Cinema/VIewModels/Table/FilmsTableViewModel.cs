﻿using System.Collections.ObjectModel;

namespace ShirkunovISP31Cinema
{
    public class FilmsTableViewModel : BaseTableViewModel<Film>
    {
        #region Поля

        /// <summary>
        /// Бизнес - логика для работы с фильмами
        /// </summary>
        private readonly IFilmService _filmService;

        #endregion

        #region Свойства BaseTableViewModel

        /// <summary>
        /// Данные таблицы
        /// </summary>
        public override ObservableCollection<Film> Data
        {
            get => Get<ObservableCollection<Film>>();
            set => Set(value);
        }

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        /// <param name="title">Заголовок таблицы</param>
        /// <param name="titleImagePath">Путь к заголовку таблицы</param>
        public FilmsTableViewModel() : base("Фильмы", "pack://application:,,,/Images/Tables/filmIcon.png")
        {
            _filmService = new FilmService();
            Data = new ObservableCollection<Film>(_filmService.FindAll());
        }

        #endregion

        #region Методы BaseTableViewModel

        /// <summary>
        /// Используется для добавления нового элемента в таблицу
        /// </summary>
        protected override void AddItem()
        {
            FilmWindow filmWindow = new FilmWindow();
            filmWindow.OpenForAdd();

            if (filmWindow.DialogResult == true && filmWindow.DataContext != null && filmWindow.DataContext is FilmWindowViewModel filmWindowViewModel)
            {
                Film newFilm = filmWindowViewModel.OriginalEntity;
                Film savedFilm = _filmService.Save(newFilm);
                Data.Add(savedFilm);
            }
        }

        /// <summary>
        /// Используется для изменения существующего элемента в таблице
        /// </summary>
        protected override void EditItem(Film entity)
        {
            FilmWindow filmWindow = new FilmWindow();
            filmWindow.OpenForEdit(entity);

            if (filmWindow.DialogResult == true && filmWindow.DataContext != null && filmWindow.DataContext is FilmWindowViewModel filmWindowViewModel)
            {
                Film newFilm = filmWindowViewModel.OriginalEntity;
                _filmService.Update(newFilm);
                Data = new ObservableCollection<Film>(_filmService.FindAll());
            }
        }

        /// <summary>
        /// Используется для удаления элемента в гриде
        /// </summary>
        /// <param name="parameter">Таблица</param>
        protected override void DeleteItemById(int id)
        {
            _filmService.DeleteById(id);
        }

        /// <summary>
        /// Используется для просмотра сущности
        /// </summary>
        protected override void OpenItem(Film entity)
        {
            FilmWindow filmWindow = new FilmWindow();
            filmWindow.OpenForView(entity);
        }

        #endregion
    }
}
