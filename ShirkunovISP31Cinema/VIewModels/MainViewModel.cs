﻿using System.Windows.Controls;

namespace ShirkunovISP31Cinema
{
    /// <summary>
    /// Наследуется от <see cref="BaseViewModel"/>
    /// </summary>
    class MainViewModel : BaseViewModel
    {
        #region Свойства

        /// <summary>
        /// Заголовок приложения
        /// </summary>
        public string Title
        {
            get => Get<string>();
            set => Set(value);
        }

        /// <summary>
        /// Ширина экрана
        /// </summary>
        public double Width
        {
            get => Get<double>();
            set => Set(value);
        }

        /// <summary>
        /// Высота экрана
        /// </summary>
        public double Height
        {
            get => Get<double>();
            set => Set(value);
        }

        /// <summary>
        /// Текст заголовка каждой страницы
        /// </summary>
        public string TextHeader
        {
            get => Get<string>();
            set => Set(value);
        }

        /// <summary>
        /// Высота логотипа
        /// </summary>
        public double HeightImageLogo
        {
            get => Get<double>();
            set => Set(value);
        }

        /// <summary>
        /// Ширина логотипа
        /// </summary>
        public double WidthImageLogo
        {
            get => Get<double>();
            set => Set(value);
        }

        /// <summary>
        /// Высота заголовка
        /// </summary>
        public double HeightHeader
        {
            get => Get<double>();
            set => Set(value);
        }

        /// <summary>
        /// Текущая страница
        /// </summary>
        public Page CurrentPage
        {
            get => Get<Page>();
            set => Set(value);
        }

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        /// <param name="context">Контекст</param>
        public MainViewModel()
        {
            Title = "Киновидеопрокат";
            TextHeader = "ООО \"КИНОВИДЕОПРОКАТ\"";
            Width = 1000;
            Height = 800;
            HeightImageLogo = 80;
            WidthImageLogo = 80;
            HeightHeader = 80;
            CurrentPage = new AnalyticalPage();
        }

        #endregion
    }
}
