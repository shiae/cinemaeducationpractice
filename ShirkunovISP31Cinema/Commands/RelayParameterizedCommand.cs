﻿using System;
using System.Windows.Input;

namespace ShirkunovISP31Cinema
{
    /// <summary>
    /// Класс, реализующий комманду с параметрами
    /// </summary>
    class RelayParameterizedCommand : ICommand
    {
        #region Приватные поля

        /// <summary>
        /// Хранит какое - либо действие с параметром
        /// </summary>
        private Action<object> _action;

        #endregion

        #region События ICommand

        /// <summary>
        /// Используется для определения того, можно ли выполнять данную команду
        /// </summary>
        public event EventHandler CanExecuteChanged;

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        /// <param name="action">Действие с параметром типа <see cref="object"/></param>
        public RelayParameterizedCommand(Action<object> action)
        {
            _action = action;
        }

        #endregion

        #region Методы ICommand

        /// <summary>
        /// True - может выполняться, иначе False.
        /// В текущей реализации он всегда может выполняться.
        /// </summary>
        /// <param name="parameter">Параметр</param>
        /// <returns>True - может выполняться, иначе False.</returns>
        public bool CanExecute(object parameter)
        {
            return true;
        }

        /// <summary>
        /// Используется для выполнения действия
        /// </summary>
        /// <param name="parameter">Параметр</param>
        public void Execute(object parameter)
        {
            _action(parameter);
        }

        #endregion
    }
}
