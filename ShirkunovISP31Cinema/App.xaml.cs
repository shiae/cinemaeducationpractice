﻿using System.Threading.Tasks;
using System;
using System.Windows;
using System.Windows.Threading;

namespace ShirkunovISP31Cinema
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        #region Свойства

        /// <summary>
        /// Контекст
        /// </summary>
        public static ApplicationDbContext DbContext => new ApplicationDbContext();

        #endregion

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            // Настраиваем главное окно приложения
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            DispatcherUnhandledException += App_DispatcherUnhandledException;
            TaskScheduler.UnobservedTaskException += OnUnobservedTaskException;

            if (!IsDataBaseConnected())
            {
                if (MessageBox.Show("Не удалось подключиться к базе данных", "Ошибка", MessageBoxButton.OK) == MessageBoxResult.OK)
                {
                    Application.Current.Shutdown();
                }
            }
        }

        /// <summary>
        /// Используется в качестве глобального обработчика событий
        /// </summary>
        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            ShowGlobalErrorMessage(((Exception)e.ExceptionObject).Message);
        }

        /// <summary>
        /// Используется в качестве глобального обработчика событий
        /// </summary>
        private void App_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            e.Handled = true;
            ShowGlobalErrorMessage(e.Exception.Message);
        }

        /// <summary>
        /// Используется когда происходит ошибка в асинхронном коде
        /// </summary>
        private void OnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs e)
        {
            e.SetObserved();
            ShowGlobalErrorMessage(e.Exception.Message);
        }

        private void ShowGlobalErrorMessage(string message)
        {
            MessageBox.Show($"Не волнуйтесь! Произошла непредвиденная ошибка приложения: {message}. Обратитесь к системному администратору!", "Непредвиденная ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private bool IsDataBaseConnected()
        {
            return DbContext.Database.Exists();
        }
    }
}
