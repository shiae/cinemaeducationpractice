﻿using System.Windows.Controls;

namespace ShirkunovISP31Cinema
{
    /// <summary>
    /// Логика взаимодействия для AnalyticalPage.xaml
    /// </summary>
    public partial class AnalyticalPage : Page
    {
        public AnalyticalPage()
        {
            InitializeComponent();
            DataContext = new AnalyticalPageViewModel();
        }
    }
}
