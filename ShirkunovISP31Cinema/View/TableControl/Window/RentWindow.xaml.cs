﻿using System.Windows;

namespace ShirkunovISP31Cinema
{
    /// <summary>
    /// Логика взаимодействия для RentWindow.xaml
    /// </summary>
    public partial class RentWindow : BaseWindow<Rent>
    {
        #region Конструкторы

        public RentWindow()
        {
            InitializeComponent();
        }

        #endregion

        #region Методы

        protected override void Open(Rent rent, OpeningRegime openingRegime)
        {
            RentWindowViewModel rentWindowViewModel = new RentWindowViewModel(rent, openingRegime);
            this.DataContext = rentWindowViewModel;
            this.ShowDialog();
        }

        #endregion
    }
}
