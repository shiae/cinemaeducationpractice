﻿using System.Windows;

namespace ShirkunovISP31Cinema
{
    /// <summary>
    /// Логика взаимодействия для FilmWindow.xaml
    /// </summary>
    public partial class FilmWindow : BaseWindow<Film>
    {
        #region Конструкторы

        public FilmWindow()
        {
            InitializeComponent();
        }

        #endregion

        #region Методы

        protected override void Open(Film film, OpeningRegime openingRegime)
        {
            FilmWindowViewModel filmWindowViewModel = new FilmWindowViewModel(film, openingRegime);
            this.DataContext = filmWindowViewModel;
            this.ShowDialog();
        }

        #endregion
    }
}
