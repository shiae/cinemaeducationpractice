﻿using System.Windows;

namespace ShirkunovISP31Cinema
{
    /// <summary>
    /// Логика взаимодействия для CinemaWindow.xaml
    /// </summary>
    public partial class CinemaWindow : BaseWindow<Cinema>
    {
        #region Конструкторы

        public CinemaWindow()
        {
            InitializeComponent();
        }

        #endregion

        #region Методы

        protected override void Open(Cinema cinema, OpeningRegime openingRegime)
        {
            CinemaWindowViewModel filmWindowViewModel = new CinemaWindowViewModel(cinema, openingRegime);
            this.DataContext = filmWindowViewModel;
            this.ShowDialog();
        }

        #endregion
    }
}
