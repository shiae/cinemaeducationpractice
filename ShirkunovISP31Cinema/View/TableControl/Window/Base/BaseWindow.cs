﻿using System.Windows;

namespace ShirkunovISP31Cinema
{
    public abstract class BaseWindow<Entity> : Window
        where Entity : BaseEntity, new()
    {
        public void OpenForView(Entity entity)
        {
            Open(entity, OpeningRegime.VIEW);
        }

        public void OpenForEdit(Entity entity)
        {
            Open(entity, OpeningRegime.EDIT);
        }

        public void OpenForAdd()
        {
            Open(new Entity(), OpeningRegime.ADD);
        }

        protected abstract void Open(Entity entity, OpeningRegime openingRegime);
    }
}
