﻿using System.Windows;

namespace ShirkunovISP31Cinema
{
    /// <summary>
    /// Логика взаимодействия для PurchaseWindow.xaml
    /// </summary>
    public partial class PurchaseWindow : BaseWindow<Purchase>
    {
        #region Конструкторы

        public PurchaseWindow()
        {
            InitializeComponent();
        }

        #endregion

        #region Методы

        protected override void Open(Purchase purchase, OpeningRegime openingRegime)
        {
            PurchaseWindowViewModel purchaseWindowViewModel = new PurchaseWindowViewModel(purchase, openingRegime);
            this.DataContext = purchaseWindowViewModel;
            this.ShowDialog();
        }

        #endregion
    }
}
