﻿using System.Windows;

namespace ShirkunovISP31Cinema
{
    /// <summary>
    /// Логика взаимодействия для IncomeWindow.xaml
    /// </summary>
    public partial class IncomeWindow : Window
    {
        public IncomeWindow()
        {
            InitializeComponent();
            DataContext = new IncomeWindowViewModel(this);
        }
    }
}
