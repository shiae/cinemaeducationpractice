﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Windows.Controls;

namespace ShirkunovISP31Cinema
{
    /// <summary>
    /// Логика взаимодействия для TableControl.xaml
    /// </summary>
    public partial class TableControl : UserControl
    {
        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public TableControl()
        {
            InitializeComponent();
        }

        #endregion

        #region Обработчики событий

        /// <summary>
        /// Используется для изменения имени в автогенерируемых столбцах
        /// </summary>
        private void DataGrid_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyDescriptor is PropertyDescriptor descriptor)
            {
                // Получаем атрибут Display, если он есть
                var displayAttribute = (DisplayAttribute)descriptor.Attributes[typeof(DisplayAttribute)];

                // Если атрибут Display установлен, то устанавливаем его значение в качестве заголовка столбца
                if (displayAttribute != null)
                {
                    e.Column.Header = displayAttribute.Name;
                }
                else
                {
                    e.Cancel = true;
                }    
            }
        }

        #endregion
    }
}
