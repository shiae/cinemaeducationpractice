﻿using System;
using System.Globalization;

namespace ShirkunovISP31Cinema
{
    public class OpeningRegimeToBooleanConverter : BaseValueConverter<OpeningRegimeToBooleanConverter>
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            OpeningRegime openingRegime = (OpeningRegime)value;
            return openingRegime == OpeningRegime.VIEW;
        }
    }
}
