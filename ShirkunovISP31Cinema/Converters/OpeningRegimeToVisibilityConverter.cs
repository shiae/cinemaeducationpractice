﻿using System;
using System.Globalization;
using System.Windows;

namespace ShirkunovISP31Cinema
{
    public class OpeningRegimeToVisibilityConverter : BaseValueConverter<OpeningRegimeToVisibilityConverter>
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                OpeningRegime openingRegime = (OpeningRegime) value;

                if (openingRegime == OpeningRegime.EDIT)
                {
                    return Visibility.Visible;
                }
                else if (openingRegime == OpeningRegime.ADD)
                {
                    return Visibility.Visible;
                }
                else
                {
                    return Visibility.Collapsed;
                }
            }

            return Visibility.Collapsed;
        }
    }

}
