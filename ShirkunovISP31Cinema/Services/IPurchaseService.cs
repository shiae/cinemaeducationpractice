﻿namespace ShirkunovISP31Cinema
{
    /// <summary>
    /// Базовый интерфейс для реализации бизнес логики по работе с <see cref="Purchase"/>
    /// </summary>
    public interface IPurchaseService : ICrudService<Purchase, int>
    {
    }
}
