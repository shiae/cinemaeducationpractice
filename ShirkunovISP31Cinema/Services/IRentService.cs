﻿namespace ShirkunovISP31Cinema
{
    /// <summary>
    /// Базовый интерфейс для реализации бизнес логики по работе с <see cref="Rent"/>
    /// </summary>
    public interface IRentService : ICrudService<Rent, int>
    {
    }
}
