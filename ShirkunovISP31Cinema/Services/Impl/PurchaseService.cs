﻿using System.Collections.Generic;
using System.Linq;

namespace ShirkunovISP31Cinema
{
    public class PurchaseService : IPurchaseService
    {
        #region Приватные поля

        /// <summary>
        /// Контекст базы данных
        /// </summary>
        private readonly ApplicationDbContext _dbContext;

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        /// <param name="filmRepository">Репозиторий по работе с фильмами</param>
        public PurchaseService()
        {
            _dbContext = App.DbContext;
        }

        #endregion

        #region Методы IFilmService

        public IEnumerable<Purchase> FindAll()
        {
            return _dbContext.Purchases.ToList();
        }

        public Purchase FindById(int id)
        {
            return _dbContext.Purchases.Find(id);
        }

        public Purchase Save(Purchase entity)
        {
            _dbContext.Purchases.Add(entity);
            _dbContext.SaveChanges();
            return entity;
        }

        public void Update(Purchase entity)
        {
            Purchase purchase = FindById((int)entity.Id);
            purchase.IdProvider = entity.IdProvider;
            purchase.IdFilm = entity.IdFilm;
            purchase.DateBuy = entity.DateBuy;
            purchase.Summ = entity.Summ;
            _dbContext.SaveChanges();
        }

        public void DeleteById(int id)
        {
            _dbContext.Purchases.Remove(FindById(id));
            _dbContext.SaveChanges();
        }

        #endregion
    }
}
