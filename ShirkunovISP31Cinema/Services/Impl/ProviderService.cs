﻿using System.Collections.Generic;
using System.Linq;

namespace ShirkunovISP31Cinema
{
    public class ProviderService : IProviderService
    {
        #region Приватные поля

        /// <summary>
        /// Контекст базы данных
        /// </summary>
        private readonly ApplicationDbContext _dbContext;

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        /// <param name="filmRepository">Репозиторий по работе с фильмами</param>
        public ProviderService()
        {
            _dbContext = App.DbContext;
        }

        #endregion

        #region Методы IFilmService

        public IEnumerable<Provider> FindAll()
        {
            return _dbContext.Providers.ToList();
        }

        public Provider FindById(int id)
        {
            return _dbContext.Providers.Find(id);
        }

        public Provider Save(Provider entity)
        {
            _dbContext.Providers.Add(entity);
            _dbContext.SaveChanges();
            return entity;
        }

        public void Update(Provider entity)
        {
            Provider provider = FindById((int)entity.Id);
            provider.NameProvider = entity.NameProvider;
            provider.INN = entity.INN;
            provider.Address = entity.Address;
            provider.Bank = entity.Bank;
            provider.Account = entity.Account;
            provider.Sign = entity.Sign;
            _dbContext.SaveChanges();
        }

        public void DeleteById(int id)
        {
            _dbContext.Providers.Remove(FindById(id));
            _dbContext.SaveChanges();
        }

        #endregion
    }
}
