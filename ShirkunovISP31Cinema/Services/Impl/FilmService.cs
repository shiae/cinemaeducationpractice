﻿using System.Collections.Generic;
using System.Linq;

namespace ShirkunovISP31Cinema
{
    /// <summary>
    /// Реализует интерфейс <see cref="IFilmService"/>
    /// </summary>
    public class FilmService : IFilmService
    {
        #region Приватные поля

        /// <summary>
        /// Контекст базы данных
        /// </summary>
        private readonly ApplicationDbContext _dbContext;

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        /// <param name="filmRepository">Репозиторий по работе с фильмами</param>
        public FilmService()
        {
            _dbContext = App.DbContext;
        }

        #endregion

        #region Методы IFilmService

        public IEnumerable<Film> FindAll()
        {
            return _dbContext.Films.ToList();
        }

        public Film FindById(int id)
        {
            return _dbContext.Films.Find(id);
        }

        public Film Save(Film entity)
        {
            _dbContext.Films.Add(entity);
            _dbContext.SaveChanges();
            return entity;
        }

        public void Update(Film entity)
        {
            Film film = FindById((int) entity.Id);
            film.NameFilm = entity.NameFilm;
            film.Author = entity.Author;
            film.Comment = entity.Comment;
            film.Producer = entity.Producer;
            film.Company = entity.Company;
            film.Year = entity.Year;
            film.Land = entity.Land;
            film.Time = entity.Time;
            film.Cost = entity.Cost;
            film.Rate = entity.Rate;
            _dbContext.SaveChanges();
        }

        public void DeleteById(int id)
        {
            _dbContext.Films.Remove(FindById(id));
            _dbContext.SaveChanges();
        }

        #endregion
    }
}
