﻿using System.Collections.Generic;
using System.Linq;

namespace ShirkunovISP31Cinema
{
    public class RentService : IRentService
    {
        #region Приватные поля

        /// <summary>
        /// Контекст базы данных
        /// </summary>
        private readonly ApplicationDbContext _dbContext;

        /// <summary>
        /// Сервис для работы с <see cref="Film"/>
        /// </summary>
        private readonly FilmService _filmService;

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        /// <param name="filmRepository">Репозиторий по работе с фильмами</param>
        public RentService()
        {
            _dbContext = App.DbContext;
            _filmService = new FilmService();
        }

        #endregion

        #region Методы IPurchaseService

        public IEnumerable<Rent> FindAll()
        {
            return _dbContext.Rents.ToList();
        }

        public Rent FindById(int id)
        {
            return _dbContext.Rents.Find(id);
        }

        public Rent Save(Rent entity)
        {
            CalculateSumm(entity);
            _dbContext.Rents.Add(entity);
            _dbContext.SaveChanges();
            return entity;
        }

        public void Update(Rent entity)
        {
            Rent rent = FindById((int)entity.Id);
            rent.DateStart = entity.DateStart;
            rent.DateStop = entity.DateStop;
            rent.Worker = entity.Worker;
            rent.PhoneWorker = entity.PhoneWorker;
            rent.Summa = entity.Summa;
            rent.Tax = entity.Tax;
            rent.IdCinema = entity.IdCinema;
            rent.IdFilm = entity.IdFilm;
            CalculateSumm(entity);
            _dbContext.SaveChanges();
        }

        public void DeleteById(int id)
        {
            _dbContext.Rents.Remove(FindById(id));
            _dbContext.SaveChanges();
        }

        private void CalculateSumm(Rent entity)
        {
            Film film = _filmService.FindById((int) entity.IdFilm);

            if (film != null)
            {
                decimal percent;

                if (film.Land.ToLower() == "сша")
                {
                    percent = 0.33M;
                }
                else if (film.Land.ToLower() == "россия")
                {
                    percent = 0.1M;
                }
                else
                {
                    percent = 0.2M;
                }

                entity.Summa = ((film.Cost / 92) * percent) * (entity.DateStop - entity.DateStart).Value.Days;
            }
        }

        #endregion
    }
}
