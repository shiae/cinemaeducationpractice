﻿using System.Collections.Generic;
using System.Linq;

namespace ShirkunovISP31Cinema
{
    /// <summary>
    /// Реализует интерфейс <see cref="ICinemaService"/>
    /// </summary>
    public class CinemaService : ICinemaService
    {
        #region Приватные поля

        /// <summary>
        /// Контекст базы данных
        /// </summary>
        private readonly ApplicationDbContext _dbContext;

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public CinemaService()
        {
            _dbContext = App.DbContext;
        }

        #endregion

        #region Методы ICinemaService

        public IEnumerable<Cinema> FindAll()
        {
            return _dbContext.Cinemas.ToList();
        }

        public Cinema Save(Cinema entity)
        {
            _dbContext.Cinemas.Add(entity);
            _dbContext.SaveChanges();
            return entity;
        }

        public void Update(Cinema entity)
        {
            Cinema cinema = FindById((int)entity.Id);
            cinema.NameCinema = entity.NameCinema;
            cinema.INNCinema = entity.INNCinema;
            cinema.Address = entity.Address;
            cinema.Chief = entity.Chief;
            cinema.PhoneChief = entity.PhoneChief;
            cinema.Owner = entity.Owner;
            cinema.PhoneOwner = entity.PhoneOwner;
            cinema.Phone = entity.Phone;
            cinema.District = entity.District;
            cinema.BankCinema = entity.BankCinema;
            cinema.AccountCinema = entity.AccountCinema;
            cinema.Capacity = entity.Capacity;
            _dbContext.SaveChanges();
        }

        public Cinema FindById(int id)
        {
            return _dbContext.Cinemas.Find(id);
        }

        public void DeleteById(int id)
        {
            _dbContext.Cinemas.Remove(FindById(id));
            _dbContext.SaveChanges();
        }

        #endregion
    }
}
