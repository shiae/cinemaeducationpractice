﻿using System.Collections.Generic;

namespace ShirkunovISP31Cinema
{
    public interface ICrudService<Entity, ID>
        where Entity : BaseEntity
    {
        /// <summary>
        /// Используется для получения списка сущностей
        /// </summary>
        /// <returns>Список сущностей</returns>
        IEnumerable<Entity> FindAll();

        /// <summary>
        /// Используется для сохранения сущности
        /// </summary>
        /// <param name="entity">Сущность</param>
        /// <returns>Сохраненная сущность</returns>
        Entity Save(Entity entity);

        /// <summary>
        /// Используется для обновления сущности
        /// </summary>
        /// <param name="entity">Сущность</param>
        void Update(Entity entity);

        /// <summary>
        /// Используется для получения сущности по ID
        /// </summary>
        /// <param name="id">ID сущности</param>
        /// <returns>Сущность</returns>
        Entity FindById(int id);

        /// <summary>
        /// Используется для удаления по ID
        /// </summary>
        /// <param name="id">ID сущности</param>
        void DeleteById(int id);
    }
}
