﻿namespace ShirkunovISP31Cinema
{
    /// <summary>
    /// Базовый интерфейс для реализации бизнес-логики по работе с <see cref="Cinema"/>
    /// </summary>
    public interface ICinemaService : ICrudService<Cinema, int>
    {

    }
}
