﻿namespace ShirkunovISP31Cinema
{
    /// <summary>
    /// Базовый интерфейс для реализации бизнес логики по работе с <see cref="Film"/>
    /// </summary>
    public interface IFilmService : ICrudService<Film, int>
    {

    }
}
